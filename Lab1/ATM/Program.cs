﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary;



namespace ATM
{
    public class Program
    {
        public static void Main(string[] args)
        {
            void printOption()
            {
                Console.WriteLine("Choose option...");
                Console.WriteLine("1. Put money to card");
                Console.WriteLine("2. Withdraw money");
                Console.WriteLine("3. Balance");
                Console.WriteLine("4. Exit");
            }

            void deposit(Account currentUser)
            {
                Console.WriteLine("How much money you want put: ?");
                double deposit = Double.Parse(Console.ReadLine());
                currentUser.setBalance(currentUser.getBalance() + deposit);
                Console.WriteLine("Your balance:" + currentUser.getBalance());
            }

            void withdraw(Account currentUser)
            {
                Console.WriteLine("How much money you want withdraw");
                double withdraw = Double.Parse(Console.ReadLine());
                if(currentUser.getBalance() > withdraw)
                {
                    Console.WriteLine("Error.Check your balance");
                }
                else
                {
                    currentUser.setBalance(currentUser.getBalance() - withdraw);
                }
                
            }
            void balance(Account currentUser)
            {
                Console.WriteLine("Your balance:" + currentUser.getBalance());
            }

            List<Account> cardHolders = new List<Account>();
            cardHolders.Add(new Account("4567901264528465", 1234, "Taras", "Stepanenko", 134.23));
            cardHolders.Add(new Account("4567894678014522", 4321, "Vasya", "Stepanchenko", 224.23));
            cardHolders.Add(new Account("45679012645284657", 1234, "Ivan", "Mydrik", 134.23));

            Console.WriteLine("Welcome to ATM Machine");
            Console.WriteLine("Enter your card number:");
            String debitCardNum = "";
            Account currentUser;

            while(true)
            {
                try
                {
                    debitCardNum = Console.ReadLine();
                    currentUser = cardHolders.FirstOrDefault(a => a.getNum() == debitCardNum);
                        if (currentUser != null) { break; }
                        else
                    {
                        Console.WriteLine("Error.Try again  ");
                    }
                }
                catch
                {
                    Console.WriteLine("Error.Try again ");
                }
            }

            Console.WriteLine("Enter your pin code: ");
            int userPin = 0;
            while (true)
            {
                try
                {
                    userPin = int.Parse(Console.ReadLine());
                    if (currentUser.getPin()== userPin) { break; }
                    else
                    {
                        Console.WriteLine("Invalid pin , try again ");
                    }
                }
                catch
                {
                    Console.WriteLine("Invalid pin , try again  ");
                }
            }
            Console.WriteLine("Hello " + currentUser.getFirstName());
            int option = 0;
                do
            {
                printOption();
                try
                {
                    option = int.Parse(Console.ReadLine());
                }
                catch
                {                   
                    
                }
                if (option == 1) { deposit(currentUser); }
                else if(option ==2) { withdraw(currentUser); }
                else if(option == 3) { balance(currentUser); }
                else if(option ==4) { break; }
                else { option = 0; }
            }
            while (option != 4);
            Console.WriteLine("Thanks , have a good day");
        }

        

    }
}